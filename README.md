# 湘潭大学 web小组 李康建
## 第一周
### 基础thinkjs
+ https://thinkjs.org/doc/index.html
### 实验：
+ 1.克隆git@gitlab.com:atestac4/xtulab_web.git项目
+ 2.在项目中新建自己的分支，使用名字的拼音作为分支名称
+ 3.在分支中新建一个nodejs项目，使用thinkjs框架，搭建一个web，具体要求如下：
    - a)首页：使用Bootstrap写一个介绍自己情况的静态页面（自己的简历）
    - b)发布简历页：用户添加简历信息，并使用post提交到后台数据库（用户无需登录即可添加简历信息）
    - c)简历浏览页：显示所有的简历信息（列表），可进行点击，进入简历详情页
    - d)简历详情页：显示简历信息
### 每做完一部分功能进行一次commit，周一晚上7：00前上传自己的分支

## 第二周
### nunjucks
+ https://nunjucks.bootcss.com/
### validator
+ https://github.com/validatorjs/validator.js
### Android设备的远程调试
+ https://developers.google.com/web/tools/chrome-devtools/remote-debugging
### iphone设备的远程调试
+ https://washamdev.com/debug-a-website-in-ios-safari-on-windows/
+ https://github.com/RemoteDebug/remotedebug-ios-webkit-adapter
### 任务
### 拆分出页面的框架，并使用模板的继承功能来实现其他页面
### 拆分出公共的页面，并使用include进行组合
### 添加简历功能：
+ 使用validator.js验证所填写的信息是否正确，例如：邮箱格式是否正确，必填项是否都填写等，并给出相应的提示
+ 使用jquery以及post方法提交数据
+ 在Action中再次使用validator验证提交的信息是否正确
+ 数据提交成功或失败给出提示
### 简历浏览页：
+ 在没有简历时，页面给出提示
+ 分页操作，每页展示10条
### 每个页面显示各自的Title和Description
### 路由：
+ 首页的路由：/，/index
+ 简历浏览页：/resume，/resume/index
+ 简历浏览页带page参数：/resume?page=xxx，/resume/index?page=xxx
+ 添加简历页：/resume/add
+ 添加简历的POST链接：/resume/add
+ 简历详情页：/resume/detail/xxx（xxx为Id）
### 判断客户端类型：PC还是mobile，根据不同的类型，显示不同的View
### mobile端UI使用mui
+ mobile提交数据使用MUI的Ajax
+ 简历浏览页 
   + 上拉加载下一页内容
   + 下拉刷新页面

## 第三周
### 训练项目
+ 1.HTML,CSS和bootstrap的使用
+ 2.数据库设计
### 任务
### 首页/简历详情页
+ 参考链接：http://cv.qiaobutang.com/lp/540588700cf2f9642cfa6830
+ 1.导航内容换成：首页，简历浏览，添加简历，关于，登录，注册，样式与参考网站一致
### 简历浏览页
+ 参考链接：http://cv.qiaobutang.com/tpl/
### 数据库设计
#### 功能：
+ 1.首页
    + a)Internet用户和注册用户均可以查看
+ 2.简历浏览
    + a)Internet用户和注册用户均可以浏览
    + b)可根据浏览量，添加时间进行排序
+ 3.查看简历
    + a)Internet用户和注册用户均可以查看
    + b)每查看一次，浏览量+1
    + c)用户自己添加的简历可进行编辑
    + d)也可查看该简历下的项目
+ 4.添加简历
    + a)只有注册用户进行登录后才能进行添加
    + b)每个用户可创建多个简历
    + c)每个简历可添加多个项目，可从已经添加的项目中选择，也可以创建新的项目
    + d)每个项目可以添加到多个简历中
+ 5.用户中心
    + a)只有注册用户进行登录后才能进行访问
    + b)可以查看自己的所有简历
+ 6.登录
+ 7.注册
### 根据上述描述的信息，设计数据库，要求如下：
+ 1.使用visio画出数据库模型
+ 2.模型中要画出所有的表，每张表所有的字段以及表之间的关联关系
+ 3.将每张表设计用word进行说明，使用如下表格
### users用户信息表
| 字段名称    | 字段类型   | 约束  |  是否为空 |  字段释义|      备注         |
| --------   | :----:    | :----:| :----:    | :----:   |      :----:      |
| id         | int(11)   | 主键  | 否        | 用户主键  |                  |
| username   |varchar(64)|       | 否        |   用户名  |                  |
|            |           |       |           |          |                  |
|            |           |       |           |          |                  |
|            |           |       |           |          |                  ||


## 第四周
### 任务
+ 1.参考谭东写的demo，完成前三周的任务。下周一晚上7:00前上传到gitlab上
+ 2.熟悉k12coder_cms系统
    + a)下载k12coder_cms项目
    + b)使用自己的名字新建分支
    + c)配置数据库，并部署运行
    + d)根据测试文档进行测试（测试文档中有些测试要求未填写，清自行根据页面功能填写）
    + e)记录测试过程中出现的问题（样式，链接，功能）
### 任务分配
+ 龙少华：在PC端使用chrome浏览器测试
+ 李康建：在mobile端使用chrome浏览器测试
+ 刘晓宇：在PC端使用 Microsoft Edge 浏览器测试
+ 谭孝阳：在mobile端使用QQ浏览器测试


## 第六周
### 训练项目
+ 1.熟悉CMSWing系统
+ 2.练习使用CMS系统的后端管理
### 练习
### 运行项目
+ 1.克隆CmsWing项目，地址：git@gitlab.com:atestac4/CmsWing.git
+ 2.创建数据库，并运行cmswing.sql文件
+ 3.新建自己的分支，并切换到自己的分支上
+ 4.修改数据库配置
+ 5.运行项目
+ 6.熟悉系统
### 使用CMS系统后台管理创建网站
+ 1.创建新的数据库，并运行cmswing_empty.sql文件
+ 2.修改数据库配置，连接新的数据库
+ 3.登录系统的管理后台
+ 4.参考k12coder_cms系统，使用后台管理创建网站，要求如下
    + a)与参考网站样式相同
    + b)添加内容以作展示
+ 5.7月6号晚上7:00前，完成以下操作：
    + a)导出数据库，把sql文件放到项目的根目录下，文件名使用自己名字的拼音命名
    + b)提交项目日志，并上传到gitlab上
    + c)完成上述两项操作后在群里通知一下
const Base = require('./base.js');
let pcmobile = require('../else/pcmobile');

module.exports = class extends Base {
  indexAction() {
    let title = '个人简历';
    this.assign('title',title);
    /*this.assign({
       title
    })*/
    if(pcmobile(this.userAgent)){
      return this.display('mobile/m_index_index');
    }else{
      return this.display('index_index'); 
    }
  }
};

const Base = require('./base.js');
let pcmobile = require('../else/pcmobile');
let validator = require('validator');

module.exports = class extends Base {
  async indexAction() { //浏览页
      const page = this.get('page');//获取当前页数
      this.assign('page',page);
      //console.log(page);
      const user = this.model('show'); // controller 里实例化模型
      const count = await user.count('id');//总条数
      const count_page = parseInt(count/10 + 1);//总页数
      this.assign('count_page',count_page);
      //console.log(count_page);
      //const data = await user.page(page,10).countSelect(); //limit语法
      const data = await user.where('id >= '+ ((page-1)*10+1) + ' AND ' + 'id <=' + page*10).select();//page页数据
      //console.log(data)
      this.assign('data',data);
    if(pcmobile(this.userAgent)){
      return this.display('mobile/m_resume_index');
    }else{
      return this.display('resume_index'); 
    }
  }


  async addAction() { //添加简历页
    if(this.isPost){ //是否有post
      let data = this.post();
      console.log(data);
      let pd = 1 //判断
      if (validator.toDate(data.info_one.date_of_birth)) {
        console.log(data.info_one.date_of_birth);
      } else{
          console.log('出生年月格式不对，格式为：××××-××-××');
          pd = 0;
      }
      if (validator.isMobilePhone(data.info_one.phone,validator.isMobilePhoneLocales[92])) {
          console.log(data.info_one.phone);
      } else{
          console.log('电话不存在');
          pd = 0;
      }
      if (pd) {
        await this.model("info_one").add(data.info_one);
        await this.model("info_two").add(data.info_two);
        await this.model("show").add(data.show);
        this.json({"succ":true});
      } else {
        this.json({"succ":false});
      }
    }else{
      if(pcmobile(this.userAgent)){
        return this.display('mobile/m_resume_add');
      }else{
        return this.display('resume_add'); 
      }
    }
  }

  async detailAction() {//详情页
    const user1 = this.model('info_one');
    const user2 = this.model('info_two');
    let get = this.get('id');
    console.log(get);
    const data1 = await user1.where({id: get}).find();//['=', posts]
    const data2 = await user2.where({id: get}).find();
    //console.log('---------------------------------------------------------');
    this.assign('data1',data1);
    this.assign('data2',data2);
    if(pcmobile(this.userAgent)){
      return this.display('mobile/m_resume_detail');
    }else{
      return this.display('resume_detail'); 
    } //this.json(data);
  }
};